// var n =2;
// function squre (num){
//     var ans = num * num;
//     return ans;
// }

// var squre2 = squre(n);
// var squre4 = squre(4)



//JavaScript program to swap two variables

//take input from the users
// let a = prompt('Enter the first variable: ');
// let b = prompt('Enter the second variable: ');

// //create a temporary variable
// let temp;

// //swap variables
// temp = a;
// a = b;
// b = temp;

// console.log(`The value of a after swapping: ${a}`);
// console.log(`The value of b after swapping: ${b}`);


//JavaScript program to swap two variables

//take input from the users
// let a = prompt('Enter the first variable: ');
// let b = prompt('Enter the second variable: ');

// //using destructuring assignment
// [a, b] = [b, a];

// console.log(`The value of a after swapping: ${a}`);
// console.log(`The value of b after swapping: ${b}`);

//JavaScript program to swap two variables

//take input from the users
// let a = parseInt(prompt('Enter the first variable: '));
// let b = parseInt(prompt('Enter the second variable: '));

// // addition and subtraction operator
// a = a + b;
// b = a - b;
// a = a - b;

// console.log(`The value of a after swapping: ${a}`);
// console.log(`The value of b after swapping: ${b}`);


// program to solve quadratic equation
// let root1, root2;

// // take input from the user
// let a = prompt("Enter the first number: ");
// let b = prompt("Enter the second number: ");
// let c = prompt("Enter the third number: ");

// // calculate discriminant
// let discriminant = b * b - 4 * a * c;

// // condition for real and different roots
// if (discriminant > 0) {
//     root1 = (-b + Math.sqrt(discriminant)) / (2 * a);
//     root2 = (-b - Math.sqrt(discriminant)) / (2 * a);

//     // result
//     console.log(`The roots of quadratic equation are ${root1} and ${root2}`);
// }

// // condition for real and equal roots
// else if (discriminant == 0) {
//     root1 = root2 = -b / (2 * a);

//     // result
//     console.log(`The roots of quadratic equation are ${root1} and ${root2}`);
// }

// // if roots are not real
// else {
//     let realPart = (-b / (2 * a)).toFixed(2);
//     let imagPart = (Math.sqrt(-discriminant) / (2 * a)).toFixed(2);

//     // result
//     console.log(
//     `The roots of quadratic equation are ${realPart} + ${imagPart}i and ${realPart} - ${imagPart}i`
//   );
// }


// value1 = Math.abs(57);
// console.log(value1); // 57

// value2 = Math.abs(0);
// console.log(value2); // 0

// value3 = Math.abs(-2);
// console.log(value3); // 2

// // Using Math.abs() with numeric non-Number
// // single item array
// value = Math.abs([-3]);
// console.log(value); // 3

// // numeric string
// value = Math.abs("-420");
// console.log(value); // 420


// using Math.acos()

// Returns arccosine for -1 <= x <=1
// var num = Math.acos(1);
// console.log(num); // 0

// var num = Math.acos(0.5);
// console.log(num); // 1.0471975511965979 (pi/3)

// var num = Math.acos(-1);
// console.log(num); // 3.141592653589793 (pi)

// Returns NaN for x < -1 or x > 1
// var num = Math.acos(100);
// console.log(num); // NaN

// var num = Math.atan2(1, 1);
// console.log(num); // 0.7853981633974483 (PI/4)

// var num = Math.atan2(4, 3);
// console.log(num); // 0.9272952180016122

// var num = Math.atan2(0, 5);
// console.log(num); // 0

// var num = Math.atan2(Infinity, 0);
// console.log(num); // 1.5707963267948966 (PI/2)

// var num = Math.atan2(-Infinity, 0);
// console.log(num); // -1.5707963267948966 (-PI/2)

// var num = Math.atan2(Infinity, -Infinity);
// console.log(num); // 2.356194490192345 (3*PI/4)


// var num = Math.atanh(0);
// console.log(num); // 0

// var num = Math.atanh(0.75);
// console.log(num); // 0.9729550745276566

// var num = Math.atanh(1);
// console.log(num); // Infinity

// var num = Math.atanh(-1);
// console.log(num); // -Infinity

// var num = Math.atanh(2);
// console.log(num); // NaN

// var num = Math.atanh(-2);
// console.log(num); // NaN

// cosine of 1 radian
// var value1 = Math.cos(1);
// console.log(value1); // Output: 0.5403023058681398

// // negative radians are allowed
// var value2 = Math.cos(-2);
// console.log(value2); // Output: -0.4161468365471424

// // Math constants can be used
// var value3 = Math.cos(Math.PI);
// console.log(value3); // Output: -1

// // custom function for angle in degrees
// function cos(degrees) {
//     var radians = (degrees * Math.PI) / 180;
//     return Math.cos(radians);
//   }
  
// cosine of 57 degrees
// value1 = cos(57);
// console.log(value1); // Output: 0.5446390350150272

//  cosine of negative degrees
// value2 = cos(-180);
// console.log(value2); // Output: -1

// value3 = cos(360);
// console.log(value3); // Output: 

// sine of 1 radian
// var value1 = Math.sin(1);
// console.log(value1); // Output: 0.8414709848078965

//  negative radians are allowed
// var value2 = Math.sin(-2);
// console.log(value2); // Output: -0.9092974268256817

//  Math constants can be used
// var value3 = Math.sin(Math.PI / 2);
// console.log(value3); // Output: 1


// Math.sinh(x) is (e**(x) - e**(-x))/2

// var num = Math.sinh(0);
// console.log(num); // 0

// var num = Math.sinh(1);
// console.log(num); // 1.1752011936438014

// var num = Math.sinh(-1);
// console.log(num); // -1.1752011936438014

// var num = Math.sinh(2);
// console.log(num); // 3.626860407847019


// Math.cosh(x) is (e**(x) + e**(-x))/2

// var num = Math.cosh(0);
// console.log(num); // 1

// var num = Math.cosh(1);
// console.log(num); // 1.5430806348152437

// var num = Math.cosh(-1);
// console.log(num); // 1.5430806348152437

// var num = Math.cosh(2);
// console.log(num); // 3.7621956910836314

// Using Math.sqrt()
// var value = Math.sqrt(16);
// console.log(value); // 4

// var value = Math.sqrt(2);
// console.log(value); // 1.4142135623730951

// var value = Math.sqrt(0);
// console.log(value); // 0

// // returns NaN for negative numbers
// var value = Math.sqrt(-2);
// console.log(value); // NaN

// simple numbers
// var num = Math.pow(5, 2);
// console.log(num); // 25

// // fractional exponents
// num = Math.pow(8, 1 / 3); // cube root
// console.log(num); // 2

// // signed base or exponents
// num = Math.pow(-2, 5);
// console.log(num); // -32

// num = Math.pow(4, -2);
// console.log(num); // 0.0625

// // -ve bases with fractional exponents return NaN
// num = Math.pow(-8, 1 / 3);
// console.log(num); // NaN